package com.fdn.domain

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@SequenceGenerator(name = "goal_seq", initialValue = 1, allocationSize = 100)
data class Goal(
        val temperature: BigDecimal,
        val date: Date,
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO, generator = "goal_seq")
        val id: Long? = null
)
