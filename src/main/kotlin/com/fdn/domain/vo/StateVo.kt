package com.fdn.domain.vo

import java.math.BigDecimal

class StateVo(
        val temperature: BigDecimal,
        val humidity: BigDecimal,
        val isActive: Boolean,
)