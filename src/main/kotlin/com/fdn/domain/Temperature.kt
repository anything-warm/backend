package com.fdn.domain

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@SequenceGenerator(name = "temperature_seq", initialValue = 1, allocationSize = 100)
data class Temperature(
        val temperature: BigDecimal,
        val date: Date,
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "temperature_seq")
        val id: Long? = null
)
