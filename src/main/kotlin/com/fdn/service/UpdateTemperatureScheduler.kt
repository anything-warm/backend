package com.fdn.service

import com.fdn.domain.Temperature
import com.fdn.domain.vo.StateVo
import com.fdn.repository.GoalRepository
import com.fdn.repository.TemperatureRepository
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*

@Service
class UpdateTemperatureScheduler(
        private val temperatureRepository: TemperatureRepository,
        private val goalRepository: GoalRepository
) {
    val logger = LoggerFactory.getLogger(this::class.java.simpleName)

    var switch: Boolean = false
    var temperature: BigDecimal = BigDecimal(20)

//    var stateRest: StateRest = Retrofit.Builder()
//            .baseUrl("http://hub63.ru:8080/")
//            .addConverterFactory(GsonConverterFactory.create())
//            .build().create(StateRest::class.java)

    @Scheduled(fixedRate = 5000)
    fun updateCurrentTemperature() {
//        val currentState = stateRest.getCurrentState().execute().body() ?: return
        temperature = if (switch) temperature.plus(BigDecimal.ONE) else temperature.minus(BigDecimal.ONE)
        val currentState = StateVo(temperature, BigDecimal.ZERO, switch)
        switch = currentState.isActive
        temperatureRepository.save(Temperature(currentState.temperature, Date()))
        logger.info("updateCurrentTemperature ${currentState.temperature}")
    }

    @Scheduled(fixedRate = 5000)
    fun updateSwitch() {
        val currentGoal = goalRepository.findFirstByDateMax() ?: return
        val currentTemperature = temperatureRepository.findFirstByDateMax() ?: return

        if (currentGoal.temperature.minus(currentTemperature.temperature) > BigDecimal(2)) {
            if (!switch) {
                logger.info("updateSwitch switchOn")
                switch = true
//            stateRest.switchOn()
            }
        }

        if (currentGoal.temperature.minus(currentTemperature.temperature) < BigDecimal(-2)) {
            if (switch) {
                logger.info("updateSwitch switchOff")
                switch = false
//            stateRest.switchOff()
            }
        }
    }
}