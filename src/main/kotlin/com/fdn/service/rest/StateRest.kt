package com.fdn.service.rest

import com.fdn.domain.vo.StateVo
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST

interface StateRest {
    @GET("state")
    fun getCurrentState(): Call<StateVo>

    @POST("switch")
    fun switchOn(): Call<Void>

    @DELETE("switch")
    fun switchOff(): Call<Void>
}