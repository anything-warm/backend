package com.fdn.service

import com.fdn.domain.Goal
import com.fdn.repository.GoalRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*

@Service
class GoalService(
        private val goalRepository: GoalRepository
) {
    val logger = LoggerFactory.getLogger(this::class.java.simpleName)

    fun getCurrentGoal(): BigDecimal {
        val temperature = goalRepository.findFirstByDateMax()?.temperature ?: BigDecimal.ZERO
        logger.info("getCurrentGoal $temperature")
        return temperature
    }

    fun saveGoal(temperature: BigDecimal): BigDecimal {
        goalRepository.save(Goal(temperature, Date()))
        logger.info("saveGoal $temperature")
        return temperature
    }
}