package com.fdn.service

import com.fdn.repository.TemperatureRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CurrentTemperatureService(
        private val temperatureRepository: TemperatureRepository
) {
    val logger = LoggerFactory.getLogger(this::class.java.simpleName)

    fun getCurrentTemperature(): BigDecimal {
        val temperature = temperatureRepository.findFirstByDateMax()?.temperature ?: BigDecimal.ZERO
        logger.info("getCurrentTemperature $temperature")
        return temperature
    }
}