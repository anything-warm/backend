package com.fdn.controller

import com.fdn.service.CurrentTemperatureService
import com.fdn.service.GoalService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
class Controller(
        private val currentTemperatureService: CurrentTemperatureService,
        private val goalService: GoalService
) {
    @GetMapping("temperature")
    fun currentTemperature() = currentTemperatureService.getCurrentTemperature()

    @PostMapping("goal")
    fun saveGoal(@RequestBody goal: BigDecimal) = goalService.saveGoal(goal)

    @GetMapping("goal")
    fun getGoal() = goalService.getCurrentGoal()

}