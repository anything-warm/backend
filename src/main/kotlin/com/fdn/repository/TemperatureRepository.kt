package com.fdn.repository

import com.fdn.domain.Temperature
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TemperatureRepository : CrudRepository<Temperature, Long> {
    @Query(value = """SELECT new com.fdn.domain.Temperature (t.temperature, t.date, t.id)
            FROM Temperature t
            WHERE t.date = (select max(date) from Temperature)""")
    fun findFirstByDateMax(): Temperature?
}