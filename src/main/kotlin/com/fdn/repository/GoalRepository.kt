package com.fdn.repository

import com.fdn.domain.Goal
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface GoalRepository : CrudRepository<Goal, Long> {
    @Query(value = """SELECT new com.fdn.domain.Goal (t.temperature, t.date, t.id)
            FROM Goal t
            WHERE t.date = (select max(date) from Goal)""")
    fun findFirstByDateMax(): Goal?
}